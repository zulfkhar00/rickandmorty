import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_app/bloc/characters_bloc.dart';
import 'screen/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rick and Morty',
      home: BlocProvider(
        create: (context) => CharactersBloc(),
        child: HomePage(),
      ),
      theme: ThemeData(
        primaryColor: Color(0xFF3D3E44),
        scaffoldBackgroundColor: Color(0xFF242830),
        accentColor: Color(0xFF3D3E44),
      ),
    );
  }
}
