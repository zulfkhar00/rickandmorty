import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/model.dart';

class DetailScreen extends StatelessWidget {
  DetailScreen({this.selectedCharacter});

  final Result selectedCharacter;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(selectedCharacter.name),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 4,
            child: Center(
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 90,
                child: Hero(
                  tag: '${selectedCharacter.id}',
                  child: CircleAvatar(
                    radius: 88,
                    backgroundImage: NetworkImage(selectedCharacter.image),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.circle,
                  color: (selectedCharacter.isAlive == Status.ALIVE)
                      ? Colors.green
                      : (selectedCharacter.isAlive == Status.DEAD)
                          ? Colors.red
                          : Colors.black,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  '${selectedCharacter.status} - ${selectedCharacter.species}',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
              ],
            ),
          ),
          Expanded(
            child: InfoCard(
              title: 'Origin location',
              subtitle: selectedCharacter.origin.name,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            height: 1,
            color: Color(0xFF505359),
          ),
          Expanded(
            child: InfoCard(
              title: 'Gender',
              subtitle: selectedCharacter.gender,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            height: 1,
            color: Color(0xFF505359),
          ),
          Expanded(
            child: InfoCard(
              title: 'Location',
              subtitle: selectedCharacter.location.name,
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(),
          ),
        ],
      ),
    );
  }
}

class InfoCard extends StatelessWidget {
  InfoCard({this.title, this.subtitle});

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(color: Color(0xFF929397)),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            subtitle,
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ],
      ),
    );
  }
}
