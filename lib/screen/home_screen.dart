import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rick_and_morty_app/bloc/characters_bloc.dart';
import '../model.dart';
import 'detail_screen.dart';
import 'package:rick_and_morty_app/model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CharactersBloc charactersBloc;

  @override
  void initState() {
    charactersBloc = BlocProvider.of<CharactersBloc>(context);
    charactersBloc.add(FetchCharactersEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rick and Morty'),
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: BlocBuilder<CharactersBloc, CharactersState>(
          builder: (context, state) {
            if (state is CharactersInitial) {
              return _buildSpinner();
            } else if (state is LoadingState) {
              return _buildSpinner();
            } else if (state is CharactersLoadedState) {
              return _buildListView(context, state.characters);
            }
          },
        ),
      ),
    );
  }

  SpinKitDoubleBounce _buildSpinner() {
    return SpinKitDoubleBounce(
      color: Colors.green,
      size: 100.0,
    );
  }

  ListView _buildListView(BuildContext context, List<Result> characters) {
    return ListView.builder(
      padding: EdgeInsets.only(top: 20),
      itemCount: characters.length,
      itemBuilder: (_, index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DetailScreen(
                  selectedCharacter: characters[index],
                ),
              ),
            );
          },
          child: Container(
            padding: EdgeInsets.only(right: 15),
            decoration: BoxDecoration(
              color: Color(0xFF3D3E44),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            height: 100,
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 35,
                    child: CircleAvatar(
                      radius: 33,
                      backgroundImage: NetworkImage(characters[index].image),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        characters[index].name,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: Colors.white)),
                            child: Icon(
                              Icons.circle,
                              size: 20,
                              color: (characters[index].isAlive == Status.ALIVE)
                                  ? Colors.green
                                  : (characters[index].isAlive == Status.DEAD)
                                      ? Colors.red
                                      : Colors.black,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '${characters[index].status} - ${characters[index].species}',
                            style: TextStyle(color: Color(0xFF9D9EA1)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(''),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        characters[index].gender,
                        style: TextStyle(color: Color(0xFF9D9EA1)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
