part of 'characters_bloc.dart';

@immutable
abstract class CharactersEvent {}

class FetchCharactersEvent extends CharactersEvent {}
