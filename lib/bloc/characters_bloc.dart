import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rick_and_morty_app/model.dart';

part 'characters_event.dart';
part 'characters_state.dart';

class CharactersBloc extends Bloc<CharactersEvent, CharactersState> {
  CharactersBloc() : super(CharactersInitial());

  @override
  Stream<CharactersState> mapEventToState(
    CharactersEvent event,
  ) async* {
    if (event is FetchCharactersEvent) {
      yield LoadingState();
      List<Result> characters = await _fetch();
      yield CharactersLoadedState(characters: characters);
    }
  }

  Future _fetch() async {
    try {
      Response response =
          await Dio().get('https://rickandmortyapi.com/api/character?page=1');

      List<Result> users = Welcome.fromJson(response.data).results;
      return users;
    } catch (e) {
      print(e);
    }
  }
}
