part of 'characters_bloc.dart';

@immutable
abstract class CharactersState {}

class CharactersInitial extends CharactersState {}

class LoadingState extends CharactersState {}

class CharactersLoadedState extends CharactersState {
  List<Result> characters;

  CharactersLoadedState({this.characters});
}
