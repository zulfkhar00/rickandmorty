import 'package:dio/dio.dart';
import 'model.dart';
import 'model.dart';

class NetworkManager {
  Future<List<Result>> getCharacters() async {
    try {
      Response response =
          await Dio().get('https://rickandmortyapi.com/api/character?page=1');

      List<Result> welcome = Welcome.fromJson(response.data).results;

      return welcome;
    } catch (e) {
      print(e);
    }
  }
}
